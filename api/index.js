require('dotenv').config()
const {
    environmentUtils:{ validateRequireEnvs }
} = require('./utils')

require('dotenv').config()

const requiredEnvs = ['PORT', 'MONGO_URI']
validateRequireEnvs(requiredEnvs)

const { helper_mongodb } = require('./helpers');

(async()=>{await helper_mongodb.connect()})()
if(+process.argv[2]){
  console.log('holas')
  require('./databases/mongo/fake')()
}
require('./server')

process.on('SIGINT', () => {
    helper_mongodb.disconnect().then((connectionState)=>{
        console.log('Database disconnect, connection state: ',connectionState)
        console.log('Closing process')
        process.exit(0)
    })

})
