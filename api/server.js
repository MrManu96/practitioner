const express = require('express')
const app = express()
const { port } = require('./config')
const router = require('./src/routes')
const cors = require('cors');

app.use(express.json())

/**/
app.use(cors())
/**/

app.use('/courses', router.rCourses)
app.use('/students', router.rStudents)
app.use('/teachers', router.rTeachers)
app.use('/users', router.rUsers)
app.use('/movimientos', router.rMovimientos)


app.listen(port, () => {
    console.log('Server listen on port ', port)
})
