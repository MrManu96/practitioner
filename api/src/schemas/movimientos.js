const Joi = require('@hapi/joi')

const schema = Joi.object({
    account: Joi.string().required(),
    accountName: Joi.string().required(),
    amount: Joi.number().required(),
    creditCardNumber: Joi.string(),
    amoucreditCardCVVnt: Joi.string().required(),
    transactionType: Joi.string().required(),
    transactionDescription: Joi.string()
})

module.exports = schema
