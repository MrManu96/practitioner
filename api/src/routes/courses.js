const express = require('express')
const router = express.Router()
const coursesSchema = require('../schemas/course')
const validate = require('../middlewares/validateData')
const {
    createOne,
    deleteOne,
    getAll,
    getOne,
    updateOne,
    assignTeacher,
    removeTeacher
} = require('../controllers/courses')

router.get('/:_id', getOne)
router.get('/', getAll)
router.post('/', validate(coursesSchema), createOne)
router.put('/:_id', validate(coursesSchema), updateOne)
router.put('/assignTeacher/:_id', assignTeacher)
router.put('/removeTeacher/:_id', removeTeacher)
router.delete('/:_id', deleteOne)

module.exports = router
