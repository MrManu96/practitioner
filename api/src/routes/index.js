const rCourses = require('./courses')
const rStudents = require('./students')
const rTeachers = require('./teachers')
const rUsers = require('./users')
const rMovimientos = require('./movimientos');

const router = {
    rCourses,
    rStudents,
    rTeachers,
    rUsers,
    rMovimientos
}

module.exports = router
