const express = require('express')
const router = express.Router()
const studentsSchema = require('../schemas/student')
const validate = require('../middlewares/validateData')
const validateAuth = require('../middlewares/validateAuth');
const {
    createOne,
    deleteOne,
    getAll,
    getOne,
    updateOne,
    assignCourse,
    removeCourse,
    count,
    getByfirstName,
    getStudentsAgeGreaterThan,
    getStudentsByCourse
} = require('../controllers/students')

//router.get('/:_id', getOne)
router.get('/', validateAuth, getAll)
router.get('/count', count)
router.get('/getByfirstName', getByfirstName)
router.get('/getStudentsAgeGreaterThan', getStudentsAgeGreaterThan)
router.get('/getStudentsByCourse', getStudentsByCourse)
router.post('/', validate(studentsSchema), createOne)
router.put('/:_id', validate(studentsSchema), validateAuth, updateOne)
router.put('/assignCourse/:_id', assignCourse)
router.put('/removeCourse/:_id', removeCourse)
router.delete('/:_id', deleteOne)

module.exports = router
