const express = require('express')
const router = express.Router()
const teachersSchema = require('../schemas/teacher')
const validate = require('../middlewares/validateData')
const {
    createOne,
    deleteOne,
    getAll,
    getOne,
    updateOne,
    getCourses
} = require('../controllers/teachers')

//router.get('/:_id', getOne)
router.get('/', getAll)
router.get('/getCourses', getCourses)
router.post('/', validate(teachersSchema), createOne)
router.put('/:_id', validate(teachersSchema), updateOne)
router.delete('/:_id', deleteOne)

module.exports = router
