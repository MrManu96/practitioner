const express = require('express')
const router = express.Router()
const studentsSchema = require('../schemas/movimientos')
const validate = require('../middlewares/validateData')
const validateAuth = require('../middlewares/validateAuth');
const {
    createOne,
    getAll
} = require('../controllers/movimientos')

//router.get('/:_id', getOne)
router.get('/', getAll)
router.post('/', validate(studentsSchema), createOne)

module.exports = router
