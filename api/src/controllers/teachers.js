const {
    mongo: {
        teachersModel,
        coursesModel
    }
} = require('../../databases')

module.exports = {
    getAll: async (req, res) => {
        const teachers = await teachersModel.find()
        res.json(teachers)
    },
    getOne: async (req, res) => {
        const { _id } = req.params
        const result = await teachersModel.findById(_id)
        res.json(result)
    },
    createOne: async (req, res) => {
        const { firstName, lastName, age } = req.body
        const newTeacher = new teachersModel({
            firstName,
            lastName,
            age
        })
        await newTeacher.save()
        res.send(`${firstName} Saved`)
    },
    updateOne: async (req, res) => {
        const { _id } = req.params
        const {firstName, lastName, age} = req.body
        const result = await teachersModel.findByIdAndUpdate(
          _id,
          {
            $set: {
              firstName,
              lastName,
              age
            }
          },
          {useFindAndModify:false}
        )
        console.log(result)
        res.send(`${firstName} Updated`)
    },
    deleteOne: async (req, res) => {
        const { _id } = req.params
        const result = await teachersModel.findByIdAndDelete(_id)
        res.json({details:result, _id:_id, deleted: "success"})
    },
    getCourses: async (req, res) => {
      const { name } = req.query;
      coursesModel
        .find({ teachers: { $not: { $size: 0 } } })
        .populate({ path: 'teachers', match: { firstName: name } })
        .exec((err, courses) => {
          if (err) {
            console.log(err);
            return res.send(err.message);
          }

          const teacherCourses = courses.filter(
            (course) => course.teachers.length > 0
          );
          res.json(teacherCourses);
        });
    }
}
