const {
    mongo: {
        coursesModel
    }
 } = require('../../databases')

module.exports = {
    getAll: async (req, res) => {
        const result = await coursesModel.find()
        res.json(result)
        /*await coursesModel
        .find()
        .populate('teachers')
        .exec((err, courses) => {
          res.json(courses)
        })*/

    },
    getOne: async (req, res) => {
        const { _id } = req.params
        const result = await coursesModel.findById(_id)
        res.json(result)
        /*await coursesModel
        .findById(_id)
        .populate('teachers')
        .exec((err, course) => {
          res.json(course)
        })*/
    },
    createOne: async (req, res) => {
        const { name } = req.body
        const newCourse = new coursesModel({name})
        await newCourse.save()
        res.send(`${name} saved`)

    },
    updateOne: async (req, res) => {
        const { _id } = req.params
        const { name } = req.body
        const result = await coursesModel.findByIdAndUpdate(
          _id,
          {
            $set: {name}
          },
          {useFindAndModify:false}
        )
        console.log(result)
        res.send(`${name} updated`)
    },
    deleteOne: async (req, res) => {
        const { _id } = req.params
        const result = await coursesModel.findByIdAndDelete(_id)
        res.json({details:result, _id:_id, deleted: "success"})
    },
    assignTeacher: async (req, res) => {
      const { _id } = req.params
      const { teacher } = req.body
      const result = await coursesModel.findByIdAndUpdate(_id, {$push:{teachers:teacher}}, {useFindAndModify:false})
      res.send(`${result.name} updated`)
    },
    removeTeacher: async (req, res) => {
      const { _id } = req.params
      const { teacher } = req.body
      const result = await coursesModel.findByIdAndUpdate(_id, {$pull:{teachers:teacher}}, {useFindAndModify:false})
      res.send(`${result.name} updated`)
    }
}
