
const {
    mongo: { studentsModel }
} = require('../../databases')

module.exports = {
    getAll: async (req, res) => {
        const students = await studentsModel.find({},{__v:0}).sort({age:1}).skip(0).populate('courses')
        res.json(students)
    },
    getOne: async (req, res) => {
        const { _id } = req.params
        const result = await studentsModel.findById(_id)
        res.json(result)
    },
    createOne: async (req, res) => {
        const { firstName, lastName, age } = req.body
        const newStudent = new studentsModel({
            firstName,
            lastName,
            age
        })
        await newStudent.save()
        res.send(`${firstName} ${lastName} saved`)

    },
    updateOne: async (req, res) => {
        const { _id } = req.params
        const { firstName, lastName, age, courses } = req.body
        const result = await studentsModel.findByIdAndUpdate(_id, {$set: {
            firstName,
            lastName,
            age,
            updatedBy:req.userData._id
        }})
        console.log(result)
        res.send(`${firstName} ${lastName} updated`)
    },
    deleteOne: async (req, res) => {
        const { _id } = req.params
        const result = await studentsModel.findByIdAndDelete(_id)
        res.json({details:result, _id:_id, deleted: "success"})
    },
    assignCourse: async (req, res) => {
      const { _id } = req.params
      const { course } = req.body
      const result = await studentsModel.findByIdAndUpdate(_id, {$push:{courses:course}}, {useFindAndModify:false})
      res.send(`${result.firstName} updated`)
    },
    removeCourse: async (req, res) => {
      const { _id } = req.params
      const { course } = req.body
      const result = await studentsModel.findByIdAndUpdate(_id, {$pull:{courses:course}}, {useFindAndModify:false})
      res.send(`${result.firstName} updated`)
    },
    count: async (req, res) => {
      const total = await studentsModel.find().countDocuments()
      res.json({total})

    },
    getByfirstName: async (req, res) => {
      const { firstName } = req.query
      const studentsFound = await studentsModel.find({firstName:{$eq:firstName}})
      res.json({studentsFound})
    },
    getStudentsAgeGreaterThan: async (req, res) => {
      const {age} = req.query
      const studentsFound = await studentsModel.find({age:{$gte:age}}).limit(2)
      res.json(studentsFound)
    },
    getStudentsByCourse: async  (req, res) => {
      const {courseName} = req.query
      studentsModel
      .find({
        courses: {$not: {$size:0}}
      },{__v:0})
      .populate({path: 'courses', match:{name:courseName}})
      .exec((err, students) => {
        if(err){
          console.log(err)
          return res.send(err.message)
        }
        const studentsByCourse = students.filter(
            student=>{return student.courses.length > 0}
          )
        res.json(studentsByCourse)

      })
    },
    removeFromCourse: async (req, res) => {
      const { _id } = req.params;
      const { course } = req.body;
      const studentUpdated = await studentsModel.findByIdAndUpdate(
        _id,
        {
          $pull: { courses: course },
        },
        { useFindAndModify: false }
      );
      res.send(`${studentUpdated.firstName} updated`);
    }
}
