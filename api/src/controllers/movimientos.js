
const {
    mongo: { movimientosModel }
} = require('../../databases')

module.exports = {
    getAll: async (req, res) => {
        const movimientos = await movimientosModel.find({},{__v:0}).limit(10)
        res.json(movimientos)
    },
    createOne: async (req, res) => {
        const {
          account,
          accountName,
          amount,
          transactionType,
          transactionDescription
        } = req.body
        const newMovimiento = new movimientosModel({
          account,
          accountName,
          amount,
          transactionType,
          transactionDescription
        })
        await newMovimiento.save()
        const result = {
          message: `${account} ${accountName} saved`,
          status: "ok"
        }
        res.json(result)

    },
    updateOne: async (req, res) => {

    },
    deleteOne: async (req, res) => {

    }
}
