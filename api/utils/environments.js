module.exports = {
    validateRequireEnvs: (requiredEnvs) => {
        for (const requiredEnv of requiredEnvs) {
            if(!process.env[requiredEnv]) throw new Error(`${requiredEnv} Must be defined on the .env file`)
        }
    }
}