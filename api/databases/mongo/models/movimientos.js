const mongoose = require('mongoose')
const { Schema } = mongoose

const schema = new Schema(
  {
    account: { type: String, required: true },
    accountName: { type: String, required: true },
    amount: { type: Number, required: true },
    creditCardNumber: { type: String, required: false },
    amoucreditCardCVVnt: { type: Number, required: false },
    transactionType: {type: String},
    transactionDescription: {type: String}
  },
  {timestamps: true}
)

schema.plugin(require('mongoose-autopopulate'))

const model =  mongoose.model('Movimientos', schema)
module.exports = model
