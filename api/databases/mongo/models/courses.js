const mongoose = require('mongoose')
const { Schema } = mongoose

const schema = new Schema(
  {
    name: { type: String, required: true },
    teachers: [{type: Schema.Types.ObjectId, ref: 'Teachers', autopopulate: false}]
  },
  {timestamps: true}
)

schema.plugin(require('mongoose-autopopulate'))

const model =  mongoose.model('Courses', schema)
module.exports = model
