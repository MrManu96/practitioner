const studentsModel = require('./models/students')
const teachersModel = require('./models/teachers')
const coursesModel = require('./models/courses')
const usersModel = require('./models/users');
const movimientosModel = require('./models/movimientos');

module.exports = {
    studentsModel,
    teachersModel,
    coursesModel,
    usersModel,
    movimientosModel
}
