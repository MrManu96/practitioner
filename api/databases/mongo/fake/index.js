
const faker = require('faker');
const { studentsModel } = require('../');
const { movimientosModel } = require('../');
module.exports = async () => {
  for (let i = 0; i < 10; i++) {
    const newStudent = new studentsModel({
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      age: faker.random.number(50)
    })
    newStudent.save()
  }
  for (let i = 0; i < 10; i++) {
    const newMovimiento = new movimientosModel({
      account: faker.finance.account(),
      accountName: faker.finance.accountName(),
      amount: faker.finance.amount(),
      creditCardNumber: faker.finance.creditCardNumber(),
      amoucreditCardCVVnt: faker.finance.creditCardCVV(),
      transactionType: faker.finance.transactionType(),
      transactionDescription: faker.finance.transactionDescription()
    })
    newMovimiento.save()
  }
}
